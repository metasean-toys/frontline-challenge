"use strict"


function cleanInput (str) {
  // if str doesn't come wrapped in parens, then wrap it in parens
  // NOTE: this is purely in expectation of bad input from the web page
  if (str[0] != '(' && str[1] != ')') {
    str = '(' + str + ')'
  }
  return str
}


function convert (str) {
  /* JS engines come with optimized parsers, 
     so no need to rebuild one, 
     just process str so the engine can parse it 
     NOTE: the regexps below maintain spaces
  */
  str = cleanInput(str)
        .replace(/\(/g, ': {') // convert (,) notation to object notation ...
        .replace(/\)/g, '}')
        .replace(/(\w+)(})/g, '$1: ""} ') // add empty values to valueless keys
        .replace(/([^\{\},]+?)([:])/g, '"$1"$2') // wrap all keys & values in quotes ...
        .replace(/([,\{]+?)([^\{\,\"\}]*?)([,\:\}]+?)/g, '$1"$2"$3')
        .replace(/(,+?)([^\{\,\"\}]*?)(,+?)/g, '$1"$2"$3')
        .replace(/",[\s]*"/g, '": "", "')
        .slice(1) // trim off starting colon

  return JSON.parse(str)
}


function stringifyElements (obj) {
  /* Take the processed object, sorted or not, 
     and output it as the specified outline 
  */
  let str = ''
  
  const stringifyElem = ({key, val, level = 0}) => {

    const bullets = '-'.repeat(level) + (level > 0 ? ' ' : '')

    let str = bullets + key.trim() + '\n'

    if (val != undefined && val != "") {
      level++
      str += processNodes(val, level)
    }

    return str
  }

  const processNodes = (node, level = 0) => Object.keys(node).reduce((str, key, index) => {
    return str += stringifyElem({ key, val: node[key], level })
  }, '')

  str = processNodes(obj)

  return str
}


function baseline (input) {

  const obj = convert(input)

  return stringifyElements(obj)

}


function bonus(input) {

  const alphaSortWithTrim = (a, b) => {
    a = a.trim()
    b = b.trim()
    return (a > b)
  }

  const processNode = (node) => Object.keys(node)
    .sort(alphaSortWithTrim)
    .reduce((newObj, key) => {
      const children = node[key] ? processNode(node[key]) : '' // sort sub-nodes
      return newObj[key] = children, newObj
    }, {})

  const obj = convert(input) // convert to fully array-based structure
  const sortedObj = processNode(obj) // sort the top level nodes

  return stringifyElements(sortedObj)
}


/* 
    If running on NODE, export functions for use in ../tests/*.js 

    This obviously would not be necessary if using gulp or webpack
    focusing on the algorithm first
*/
if ((typeof process !== 'undefined') && (process.release.name == 'node')) {
  module.exports = {convert, baseline, bonus}
}
